const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet'); // éviter la faille XSS

// Je crée une instance de mon serveur express
const app = express();

// Lecture du port depuis le fichier .env
const port = 3001;

// Importation des routes
const userRoutes = require('./routes/userRoutes');
const taskRoutes = require('./routes/taskRoutes');

app.use(cors());

//get node env
const env = process.env.NODE_ENV || "development";

// Utilise Helmet pour éviter les problèmes de sécurité avec l'option d'évaluation non sécurisée (attaque XSS)
app.use(
    helmet.contentSecurityPolicy({
        directives: {
          "default-src": ["'self'"],
          "img-src": ["https://http.cat/images/404.jpg 'self'", "data:"],
          "script-src": ["'self'", "'unsafe-eval'"],
          "object-src": ["'none'"],
          "script-src-elem": ["'self'", "'unsafe-eval'"]
        },
      })
);

// indiquer à express qu'on peut séparer les données au format JSON
app.use(express.json())

// Middleware pour gérer CORS
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());

app.use('/api/', userRoutes);
app.use('/api/', taskRoutes);

// Démarrer le serveur
app.listen(port, function () {
    console.log(`Serveur démarré sur le port ${port} ...............................................................`);
});

module.exports = app;
