const autocannon = require('autocannon');

// Configuration de la requête
const opts = {
  url: 'http://localhost:3000/api/task/create', // L'URL de l'endpoint de création de tâches
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({ // Corps de la requête avec des données aléatoires
    description: Math.random().toString(36).substring(7),
    completed: Math.random() < 0.5,
    owner: 1 // Remplacez par l'ID de l'utilisateur approprié
  }),
  connections: 10, // Nombre de connexions simultanées
  pipelining: 1, // Nombre de requêtes en attente de réponse avant d'envoyer de nouvelles requêtes
  duration: 10 // Durée du test en secondes
};

// Lancement du test de charge avec Autocannon
autocannon(opts, (err, result) => {
  if (err) {
    console.error('Erreur lors du test de charge:', err);
    return;
  }

  // Affichage des résultats du test
  console.log(autocannon.printResult(result));
});
