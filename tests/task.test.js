const request = require('supertest');
const app = require('../index');

/*
* Test d'intégration avec Jest et Supertest
*/
describe('Task Operations', () => {
    let authToken = "";

    beforeAll(async () => {
        const loginResponse = await request(app)
            .post("/api/login")
            .send({ email: process.env.TEST_USER, password: process.env.TEST_PASSWORD });

        authToken = loginResponse.body.token;
        expect(loginResponse.status).toBe(200);
    });

    it('ajouter une tache avec succès', async () => {
        // Définir les données de la tâche à envoyer
        const taskData = {
            description: 'Task de test',
            completed: false,
            owner: 10, // Mettez ici l'ID de l'utilisateur associé
        };
        console.log("Envoi de la requête pour créer une tâche... :", taskData);
        console.log("Token avant l'envoi de la requête :", authToken);


        const taskResponse = await request(app)
            .post('/api/task/create')
            .set('Authorization', `Bearer ${authToken}`)
            .send(taskData);

        console.log("Réponse de la requête :", taskResponse.status);
        expect(taskResponse.status).toBe(201);
    });


    // // Test d'intrusion
    // it('Tentative de création de tâche sans authentification', async () => {
    //     const taskData = {
    //         description: 'Tentative de création de tâche sans authentification',
    //         completed: false,
    //         owner: 10,
    //     };

    //     const response = await request(app)
    //         .post('/api/task/create')
    //         .send(taskData);

    //     // Vérifier que la réponse est un code 401 (non autorisé)
    //     expect(response.status).toBe(401);
    // });

    // it('Tentative de création de tâche avec un token non valide', async () => {
    //     const taskData = {
    //         description: 'Tentative de création de tâche avec un token non valide',
    //         completed: false,
    //         owner: 10,
    //     };

    //     const response = await request(app)
    //         .post('/api/task/create')
    //         .set('Authorization', 'Bearer TokenNonValide')
    //         .send(taskData);

    //     // Vérifier que la réponse est un code 401 (non autorisé)
    //     expect(response.status).toBe(401);
    // });

    // it('Tentative de création de tâche avec des données malveillantes', async () => {
    //     const maliciousData = {
    //         description: 'Tâche malveillante',
    //         completed: false,
    //         owner: 10,
    //         maliciousField: '<script>alert("attaque XSS")</script>',
    //     };
    
    //     const response = await request(app)
    //         .post('/api/task/create')
    //         .send(maliciousData);
    
    //     // Vérifier que la réponse est un code 400 (mauvaise requête)
    //     expect(response.status).toBe(400);
    // });

    // it('Tentative d\'injection SQL lors de la création de tâche', async () => {
    //     // Données de la tâche avec une tentative d'injection SQL
    //     const taskData = {
    //         description: 'Tâche normale',
    //         completed: false,
    //         owner: 10,
    //         maliciousField: `'; DROP TABLE users; --`, // Injection SQL
    //     };
    
    //     // Envoi de la requête avec les données malveillantes
    //     const response = await request(app)
    //         .post('/api/task/create')
    //         .send(taskData);
    
    //     // Vérification que la réponse est un code 400 (mauvaise requête) ou un autre code approprié
    //     expect(response.status).toBe(400);
    // });
});