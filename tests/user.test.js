const request = require('supertest');
const app = require('../index'); 

/*
* Test d'integration avec Jest et Supertest
*/
describe('User Operations', () => {

    // Test d'intégration avec Jest et Supertest pour tester la route POST /user/register
    it("créer un nouvel utilisateur", async () => {
        const userData = {
            fullName: 'Namadia Smith',
            age: 27,
            email: 'namadia@example.com',
            password: 'motdepassesecurise'
        };

        const response = await request(app)
            .post('/api/user/register')
            .send(userData)
        
        expect(response.statusCode).toBe(201);
    });


    it("renvoyer un message d'ereur 400 si des données invalides sont fournies", async () => {
        const userData = {
            fullName: 'Jane Doe',
            age: 'pas un nombre', // Âge invalide
            email: 'emaillinvalide', // Email invalide
            password: 'court' // Mot de passe invalide
        };

        const response = await request(app)
            .post('/api/user/register')
            .send(userData)
            .expect(400);

        // Vérification que la réponse contient un message d'erreur
        expect(response.body).toHaveProperty('error');
    });


    it("renvoyer un message d'erreur 400 si la longueur du mot de passe est inférieure à 8 caractères", async () => {
        const userData = {
            fullName: 'Jane Doe',
            age: 30,
            email: 'jane.doe@example.com',
            password: 'court' // Longueur du mot de passe inférieure à 8 caractères
        };

        const response = await request(app)
            .post('/api/user/register')
            .send(userData)
            .expect(400);

        // Vérification que la réponse contient un message d'erreur
        expect(response.body).toHaveProperty('error');
    });
});