// module.exports = {
//     input: {
//         type: 'object', // Type d'entrée (objet pour représenter une tâche ou un utilisateur)
//         properties: {
//             // Propriétés de l'objet Task
//             description: {
//                 type: 'string', // Description de la tâche (chaîne de caractères)
//                 maxLength: 255, // Longueur maximale de la description
//             },
//             completed: {
//                 type: 'boolean', // État de complétion de la tâche (booléen)
//             },
//             owner: {
//                 type: 'integer', // ID de l'utilisateur propriétaire de la tâche (entier)
//             },
//             // Propriétés de l'objet User
//             fullName: {
//                 type: 'string', // Nom complet de l'utilisateur (chaîne de caractères)
//             },
//             age: {
//                 type: 'integer', // Âge de l'utilisateur (entier)
//             },
//             email: {
//                 type: 'string', // Adresse email de l'utilisateur (chaîne de caractères)
//                 format: 'email', // Format d'email valide
//             },
//             password: {
//                 type: 'string', // Mot de passe de l'utilisateur (chaîne de caractères)
//                 minLength: 8, // Longueur minimale du mot de passe
//                 maxLength: 255, // Longueur maximale du mot de passe
//             },
//         },
//         required: ['description', 'completed', 'owner', 'fullName', 'age', 'email', 'password'], // Champs obligatoires
//     },
//     iterations: 10000, // Nombre d'itérations de fuzzing à exécuter
//     maxTime: 60000, // Durée maximale de chaque itération (en millisecondes)
//     environment: {
//         // Configuration de l'environnement d'exécution (variables d'environnement, options de ligne de commande, etc.)
//     },
//     mutationStrategy: 'random', // Stratégie de mutation à utiliser
// };