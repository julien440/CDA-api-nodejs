# API - Gestion de tâches + utilisateur

Mon projet est une API de gestion des tâches, qui permet aux utilisateurs de créer, visualiser, mettre à jour et supprimer des tâches. Il y a également un système de middleware si vous souhaitez vous en inspirer en terme de code.


## Les endpoints pour les tâches

| HTTP | ROUTE     | DESCRIPTION                |
| :-------- | :------- | :------------------------- |
| `GET` | `/api/tasks` | Permet de récupérer l'ensemble des tâches |
| `GET` | `/api/task/:id` | Permet de récupérer une tâche pour un utilisateur spécifique |
| `POST` | `/api/task/create` | Permet de créer une tâche |
| `GET` | `/api/tasks/:id` | Permet de récupérer une tâche par son ID |
| `DELETE` | `/api/tasks/delete/:id` | Permet de supprimer une tâche par son ID |
| `PATCH` | `/api/tasks/:id` | Permet de modifier une tâche par son ID |
| `GET` | `/api/users/:userId/tasks` | Permet de récupérer toutes les tâches pour un utilisateur |


## Les endpoints pour les utilisateurs

| HTTP | ROUTE     | DESCRIPTION                |
| :-------- | :------- | :------------------------- |
| `POST` | `/api/user/register` | Permet de créer un nouvel utilisateur |
| `GET` | `/api/users` | Permet de récupérer tous les utilisateurs |
| `GET` | `/api/users/:id` | Permet de récupérer un utilisateur en fonction de son identifiant |
| `DELETE` | `/api/users/delete/:id` | Permet de supprimer un utilisateur en fonction de son identifiant |
| `UPDATE` | `/api/users/:id` | Permet de supprimer une tâche par son ID |
| `POST` | `/api/login` | Permet d'établir une connexion pour un utilisateur |


## Test

Vous pouvez lancer des tests unitaires en utilisant la commande suivante : 

```bash
  npm test
```

## Installation

Pour installer le projet, il suffit de faire un clone du projet en lançant la commande :

```bash
  git clone  https://gitlab.com/julien440/CDA-api-nodejs.git
```

Puis de vous rendre dans le projet et lancer la commande : 
``` bash
  npm install
```

Il est nécessaire d'avoir un service web comme WAMPP 