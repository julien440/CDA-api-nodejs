// const { fuzzFunction } = require('dharma');

// // Importer le contrôleur de création de tâches
// const taskController = require('../controllers/taskCtrl');

// // Définir le scénario de fuzzing pour la création de tâches
// const fuzzCreateTask = async (input) => {
//     try {
//         // Extraire les données de l'entrée de fuzzing
//         const { description, completed, owner } = input;

//         // Envoyer les données de fuzzing au contrôleur de création de tâches
//         const newTask = await taskController.createTask({ 
//             body: { 
//                 description, 
//                 completed, 
//                 owner,
//             } 
//         });

//         // Retourner la nouvelle tâche créée
//         return newTask;
//     } catch (error) {
//         // En cas d'erreur, lever l'erreur
//         throw error;
//     }
// };

// // Lancer le fuzzing avec Dharma
// fuzzFunction(fuzzCreateTask);